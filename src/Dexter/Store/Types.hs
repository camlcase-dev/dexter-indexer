{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE LambdaCase                 #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE StandaloneDeriving         #-}
{-# OPTIONS_GHC -fno-warn-orphans       #-}

module Dexter.Store.Types where

import Data.Aeson (ToJSON(toJSON), FromJSON(parseJSON), (.=), (.:), object,
                   withObject, withScientific, Value(Object))
import Data.Aeson.Types (Parser, typeMismatch)
import qualified Data.HashMap.Strict as H
import Data.Int (Int64)
import qualified Data.Scientific
import Data.Text (Text)
import Data.Typeable (Typeable)
import Data.Word (Word64)

import Database.PostgreSQL.Simple.ToField (ToField(toField), toJSONField)
import Database.PostgreSQL.Simple.FromField (FromField(fromField), fromJSONField)
import Database.PostgreSQL.Simple.ToRow (ToRow(toRow))
import Database.PostgreSQL.Simple.FromRow (FromRow(fromRow), field)

import GHC.Generics (Generic)

import Tezos.Client.Types (BlockHash(..), ContractId(..), Expression(..), Mutez(..), PositiveBignum(..), Unistring(..))

-- =============================================================================
-- SqlKey
-- =============================================================================

-- | Class for types of primary keys
class IsSqlKey a where
  keyFromSqlKey :: SqlKey -> a
  keyToSqlKey :: a -> SqlKey

-- | Each key/id type should be a newtype around 'SqlKey' and have an instance
-- of 'IsSqlKey'.
newtype SqlKey = SqlKey Int64 deriving (Eq, Generic, Ord, Read, Show, Typeable)  

instance ToJSON SqlKey where
  toJSON (SqlKey i) = toJSON i

instance FromJSON SqlKey where
  parseJSON = withInteger "SqlKey" $ \i -> pure $ SqlKey i

instance ToField SqlKey where
  toField (SqlKey key) = toField key

instance FromRow SqlKey where
  fromRow = SqlKey <$> field

instance FromField SqlKey where
  fromField f dat = SqlKey <$> fromField f dat 

fromSqlKey :: SqlKey -> Int64
fromSqlKey (SqlKey i) = i

toSqlKey :: Int64 -> SqlKey
toSqlKey = SqlKey

withInteger
  :: (Bounded n, Integral n)
  => String
  -> (n -> Parser a)
  -> Value
  -> Parser a
withInteger expected f x = withScientific expected f' x
  where
    f' = maybe (typeMismatch expected x) f . Data.Scientific.toBoundedInteger

-- =============================================================================
-- SqlEntity
-- =============================================================================

-- | An SqlEntity is a value that can be stored in PostgreSQL that has a key.
-- | A pair of a key and corresponding value
data SqlEntity a b where
  SqlEntity :: IsSqlKey a =>
    { entityKey :: a
    , entityVal :: b
    } -> SqlEntity a b

deriving instance (Read a, IsSqlKey a, Read b) => Read (SqlEntity a b)
deriving instance (Show a, Show b)             => Show (SqlEntity a b)
deriving instance (Ord a, Ord b)               => Ord (SqlEntity a b)
deriving instance (Typeable a, Typeable b)     => Typeable (SqlEntity a b)

instance (Eq a, Eq b) => Eq (SqlEntity a b) where
  (==) (SqlEntity a b) (SqlEntity a' b') = a == a' && b == b'

instance (ToJSON a, ToJSON b) => ToJSON (SqlEntity a b) where
  toJSON (SqlEntity a b) =
    object
      [ "key"   .= a
      , "value" .= b
      ]

instance (FromJSON a, FromJSON b, IsSqlKey a) => FromJSON (SqlEntity a b) where
  parseJSON = withObject "SqlEntity" $ \o ->
    SqlEntity
      <$> o .: "key"
      <*> o .: "value"

instance (ToRow a, ToRow b) => ToRow (SqlEntity a b) where
  toRow (SqlEntity key' val) = (toRow key') ++ (toRow val)

instance (IsSqlKey a, FromField a, FromRow b) => FromRow (SqlEntity a b) where
  fromRow = SqlEntity <$> field <*> fromRow


-- =============================================================================
-- ExchangeContract
-- =============================================================================

-- | The details of a Dexter Exchange Contract from the Tezos Blockchain
data ExchangeContract =
  ExchangeContract
    { exchangeName             :: Text
    , exchangeContractId       :: ContractId
    , exchangeInitialBlockHash :: BlockHash
    , tokenContractId          :: ContractId -- | The token contract that the exchange trades for
    } deriving (Eq, Show, Generic)

instance ToRow ExchangeContract where
  toRow (ExchangeContract name' exchangeContractId' exchangeInitialBlockHash' tokenContractId') =
    [toField name', toField exchangeContractId',
     toField exchangeInitialBlockHash', toField tokenContractId']

instance FromRow ExchangeContract where
  fromRow = ExchangeContract <$> field <*> field <*> field <*> field

instance FromJSON ExchangeContract where
  parseJSON = withObject "ExchangeContract" $ \o ->
    case H.toList o of
      [(key', Object o2)] ->
        ExchangeContract key'
        <$> o2 .: "exchange-contract-id"
        <*> o2 .: "exchange-initial-block-hash"
        <*> o2 .: "token-contract-id"
      _ -> fail "Not able to parse ExchangeContract."

newtype ExchangeContractId =
  ExchangeContractId SqlKey
  deriving (Eq, Generic, Ord, Read, Show, Typeable, ToJSON, FromJSON, Typeable)

instance IsSqlKey ExchangeContractId where
  keyFromSqlKey key = (ExchangeContractId key)
  keyToSqlKey (ExchangeContractId key) = key

instance FromRow ExchangeContractId where
  fromRow = ExchangeContractId <$> field

instance ToField ExchangeContractId where
  toField (ExchangeContractId exchangeContractId') = toField exchangeContractId'

instance FromField ExchangeContractId where
  fromField f dat = ExchangeContractId <$> fromField f dat 

-- =============================================================================
-- ExchangeTransaction
-- =============================================================================

-- | A single transaction from the Tezos blockchain for a particular Dexter
-- Exchange.
data ExchangeTransaction =
  ExchangeTransaction
    { trBlockHash    :: BlockHash
    , trSource       :: ContractId
    , trLevel        :: Int64
    , trFee          :: Mutez
    , trCounter      :: PositiveBignum
    , trGasLimit     :: PositiveBignum
    , trStorageLimit :: PositiveBignum
    , trAmount       :: Mutez
    , trParameters   :: Maybe Expression
    , trExchangeContractId :: ExchangeContractId
    } deriving (Eq, Show)

instance ToRow ExchangeTransaction where
  toRow (ExchangeTransaction blockHash' source' level' fee' counter' gasLimit'
         storageLimit' amount' parameters' exchangeContractId') =
    [ toField blockHash'
    , toField source'
    , toField level'
    , toField fee'
    , toField counter'
    , toField gasLimit'
    , toField storageLimit'
    , toField amount'
    , toField parameters'
    , toField exchangeContractId'
    ]

instance FromRow ExchangeTransaction where
  fromRow = ExchangeTransaction <$> field <*> field <*> field <*> field <*> field <*>
    field <*> field <*> field <*> field <*> field

newtype ExchangeTransactionId =
  ExchangeTransactionId SqlKey
  deriving (Eq, Generic, Ord, Read, Show, Typeable, ToJSON, FromJSON, Typeable)

instance IsSqlKey ExchangeTransactionId where
  keyFromSqlKey key = (ExchangeTransactionId key)
  keyToSqlKey (ExchangeTransactionId key) = key

instance FromRow ExchangeTransactionId where
  fromRow = ExchangeTransactionId <$> field

instance ToField ExchangeTransactionId where
  toField (ExchangeTransactionId exchangeTransactionId') = toField exchangeTransactionId'

instance FromField ExchangeTransactionId where
  fromField f dat = ExchangeTransactionId <$> fromField f dat 

-- =============================================================================
-- ExchangeBalance
-- =============================================================================

-- | The balance of a dexter exchange at any particular blockhash.
data ExchangeBalance =
  ExchangeBalance
    { ebBlockHash  :: BlockHash
    , ebLevel      :: Int64
    , ebLiquidity  :: PositiveBignum
    , ebMutez      :: Mutez
    , ebToken      :: PositiveBignum
    , ebExchangeId :: ExchangeContractId
    } deriving (Eq, Show)

instance ToRow ExchangeBalance where
  toRow (ExchangeBalance blockHash' level' liquidity' mutez' token' exchangeContractId') =
    [ toField blockHash'
    , toField level'
    , toField liquidity'
    , toField mutez'
    , toField token'
    , toField exchangeContractId'
    ]

instance FromRow ExchangeBalance where
  fromRow = ExchangeBalance <$> field <*> field <*> field <*> field <*> field <*>
    field

newtype ExchangeBalanceId =
  ExchangeBalanceId SqlKey
  deriving (Eq, Generic, Ord, Read, Show, Typeable, ToJSON, FromJSON, Typeable)

instance IsSqlKey ExchangeBalanceId where
  keyFromSqlKey key = (ExchangeBalanceId key)
  keyToSqlKey (ExchangeBalanceId key) = key

instance FromRow ExchangeBalanceId where
  fromRow = ExchangeBalanceId <$> field

instance ToField ExchangeBalanceId where
  toField (ExchangeBalanceId exchangeTransactionId') = toField exchangeTransactionId'

instance FromField ExchangeBalanceId where
  fromField f dat = ExchangeBalanceId <$> fromField f dat 

-- =============================================================================
-- Account
-- =============================================================================

-- | An account that has made an exchange transaction.
newtype Account =
  Account
    { accountAddress :: ContractId
    } deriving (Eq, Show, Ord)

instance ToRow Account where
  toRow (Account address') =
    [toField address']

instance FromRow Account where
  fromRow = Account <$> field

newtype AccountId =
  AccountId SqlKey
  deriving (Eq, Generic, Ord, Read, Show, Typeable, ToJSON, FromJSON, Typeable)

instance IsSqlKey AccountId where
  keyFromSqlKey key = (AccountId key)
  keyToSqlKey (AccountId key) = key

instance FromRow AccountId where
  fromRow = AccountId <$> field

instance ToField AccountId where
  toField (AccountId accountId') = toField accountId'

instance FromField AccountId where
  fromField f dat = AccountId <$> fromField f dat 

-- =============================================================================
-- AccountBalance
-- =============================================================================

-- | The details of an account's balance at a particular blockhash
data AccountBalance =
  AccountBalance
    { accountBalanceBlockHash  :: BlockHash
    , accountBalanceLevel      :: Int64
    , accountBalanceLiquidity  :: PositiveBignum
    , accountBalanceMutez      :: Mutez
    , accountBalanceToken      :: PositiveBignum
    , accountBalanceAccountId  :: AccountId
    , accountBalanceExchangeId :: ExchangeContractId
    } deriving (Eq, Show)

instance ToRow AccountBalance where
  toRow (AccountBalance blockHash' level' liquidity' mutez' token' accountId' exchangeContractId') =
    [ toField blockHash'
    , toField level'
    , toField liquidity'
    , toField mutez'
    , toField token'
    , toField accountId'
    , toField exchangeContractId'
    ]

instance FromRow AccountBalance where
  fromRow = AccountBalance <$> field <*> field <*> field <*> field <*> field <*>
    field <*> field

newtype AccountBalanceId =
  AccountBalanceId SqlKey
  deriving (Eq, Generic, Ord, Read, Show, Typeable, ToJSON, FromJSON, Typeable)

instance IsSqlKey AccountBalanceId where
  keyFromSqlKey key = (AccountBalanceId key)
  keyToSqlKey (AccountBalanceId key) = key

instance FromRow AccountBalanceId where
  fromRow = AccountBalanceId <$> field

instance ToField AccountBalanceId where
  toField (AccountBalanceId accountBalanceId') = toField accountBalanceId'

instance FromField AccountBalanceId where
  fromField f dat = AccountBalanceId <$> fromField f dat 

-- =============================================================================
-- ToField and FromField instances for Tezos.Client.Types
-- =============================================================================

instance ToField Unistring where
  toField = \case
    UnistringText t       -> toField t
    InvalidUtf8String _ws -> toField ("InvalidUtf8String" :: Text)

instance FromField Unistring where
  fromField f dat = UnistringText <$> fromField f dat

instance ToField BlockHash where
  toField (BlockHash blockHash') = toField blockHash'

instance FromField BlockHash where
  fromField f dat = BlockHash <$> fromField f dat

instance ToField ContractId where
  toField (ContractId contractId') = toField contractId'

instance FromField ContractId where
  fromField f dat = ContractId <$> fromField f dat

instance ToField PositiveBignum where
  toField (PositiveBignum positiveBignum') = toField positiveBignum'

instance FromField PositiveBignum where
  fromField f dat = PositiveBignum <$> fromField f dat

instance ToField Mutez where
  toField (Mutez mutez') = toField mutez'

instance FromField Mutez where
  fromField f dat = Mutez . (fromIntegral :: Int64 -> Word64) <$> fromField f dat

instance ToField Expression where
  toField = toJSONField

instance FromField Expression where
  fromField = fromJSONField
