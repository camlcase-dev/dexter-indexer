{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE OverloadedStrings     #-}

module Dexter.Store.Config where

import           Data.Aeson (FromJSON(parseJSON), withObject, (.:), (.:?))
import qualified Data.Text as T
import           Data.Word  (Word16)

import           Dexter.Store.Types (ExchangeContract)

import           GHC.Generics (Generic)

import           Servant.Client (Scheme(Http, Https))

-- | Top level data type of the yaml file.
data DexterStoreManagerConfig =
  DexterStoreManagerConfig
    { database  :: PostgreSQLConfig
    , contracts :: [ExchangeContract]
    , tezosCli  :: TezosClientConfig
    } deriving (Eq, Show, Generic)

instance FromJSON DexterStoreManagerConfig where
  parseJSON = withObject "DexterStoreManagerConfig" $ \o ->
    DexterStoreManagerConfig <$> o .: "database"
                             <*> o .: "contracts"
                             <*> o .: "tezos-cli"

-- | Tezos Client config
data TezosClientConfig = TezosClientConfig
  { tezosHost     :: String
  , tezosPort     :: Int
  , tezosProtocol :: Scheme
  } deriving (Eq, Show)

instance FromJSON TezosClientConfig where
  parseJSON = withObject "TezosClientConfig" $ \o -> do
    mProtocol <- fmap T.toLower <$> o .:? "protocol"
    let protocol =
          case mProtocol of
            Just "http"  -> Http
            Just "https" -> Https
            _            -> Http    
    TezosClientConfig <$> o .: "host"
                      <*> o .: "port"
                      <*> pure protocol

-- | PostgreSQL config
data PostgreSQLConfig = PostgreSQLConfig
  { psqlHost        :: String
  , psqlPort        :: Word16
  , psqlUser        :: String
  , psqlDBName      :: String
  , psqlPassword    :: String
  } deriving (Eq, Show)

instance FromJSON PostgreSQLConfig where
  parseJSON = withObject "PostgreSQLConfig" $ \o ->
        PostgreSQLConfig <$> o .: "host"
                         <*> o .: "port"
                         <*> o .: "user"
                         <*> o .: "dbname"
                         <*> o .: "password"
